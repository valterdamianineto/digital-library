// create reference to fetch information from database
var db = firebase.database();
var ref = db.ref("books");

function bookBoxClone() {
    // get information from the database according to the chosen values
    ref.orderByChild("pages").limitToLast(56).on("child_added", function(snapshot) {
        let key = snapshot.key;
        // var childKey = snapshot.child("name/last").key;
        var cardSrc = snapshot.val().bookCover;
        var cardName = snapshot.val().bookName;
        var container = document.getElementById("box");

        //Count the total elements and add one more to what will be added
        var elementNumber = container.childElementCount + 1;

        // create elements in HTML
        var nameBox = document.createElement("p");
        var div1 = document.createElement("div");
        var div2 = document.createElement("div");
        var div3 = document.createElement("div");
        var img = document.createElement("img");

        // pass the name of the classes to each of the elements
        div1.className = "card";
        div2.className = "imgBox";
        div3.className = "infoBox";
        img.className = "imgTag";

        // insert SRC and ALT parameters into IMG tag
        img.htmlFor = 'p_' + elementNumber;
        img.src = cardSrc;
        img.alt = cardName;

        // enter the name of the book through a variable, as well as the class name, tag name and its type
        nameBox.textContent = cardName;
        nameBox.className = "nameBox";
        nameBox.name = "inp";
        nameBox.type = "text";

        //   Include an ID for each P, adding "elementNumber" to change the id in each tag
        nameBox.id = 'nameBox' + elementNumber;

        // insert each of the elements in the HTML page
        container.appendChild(div1);
        div1.appendChild(div2);
        div2.appendChild(img);
        div1.appendChild(div3);
        div3.appendChild(nameBox);

        // open modal with selected book information
        div1.onclick = function openModal() {
            location.href = ('../book-info-page/book-info-page.html?' + key);
        }
    })
}
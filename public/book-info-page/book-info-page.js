let url = window.location.href;
url = url.split('?');
url = url[1]
let key = url

var db = firebase.database();
var ref = db.ref("books/");

ref.orderByChild("pages").limitToLast(56).on("child_added", function(snapshot) {
    var id = snapshot.key;
    if (key == id) {
        let author = snapshot.val().author;
        let bookCover = snapshot.val().bookCover;
        let bookName = snapshot.val().bookName;
        let bookNumber = snapshot.val().bookNumber
        bookPdf = snapshot.val().bookPdf;
        let genre = snapshot.val().genreSelect;
        let language = snapshot.val().language;
        let pages = snapshot.val().pages;
        let publishingCompany = snapshot.val().publishingCompany;
        let serie = snapshot.val().serie
        let synopsis = snapshot.val().synopsis;
        let yearOfEdition = snapshot.val().yearOfEdition;

        document.getElementById("bookName").innerHTML = bookName;
        document.getElementById("serie").innerHTML = serie;
        document.getElementById("bookNumber").innerHTML = bookNumber;
        document.getElementById("pages").innerHTML = pages;
        document.getElementById("yearOfEdition").innerHTML = yearOfEdition;
        document.getElementById("author").innerHTML = author;
        document.getElementById("language").innerHTML = language;
        document.getElementById("genre").innerHTML = genre;
        document.getElementById("publishingCompany").innerHTML = publishingCompany;
        document.getElementById("synopsis").innerHTML = synopsis;
        document.getElementById("bookCover").src = bookCover;
    }
})

function read() {
    window.open(bookPdf);
}
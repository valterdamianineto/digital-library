var bookName = document.getElementById('bookName');
var serie = document.getElementById('serie');
var bookNumber = document.getElementById('bookNumber');
var author = document.getElementById('author');
var pages = document.getElementById('pages');
var language = document.getElementById('language');
var yearOfEdition = document.getElementById('yearOfEdition');
var publishingCompany = document.getElementById('publishingCompany');
var synopsis = document.getElementById('synopsis');
var save = document.getElementById('save')
var genreSelect = document.getElementById('genreSelect');
var cover = document.getElementById('cover')
var book = document.getElementById('book');
var coverProgress = document.getElementById('coverProgress');
var bookProgress = document.getElementById('bookProgress')

function saveBtn() {
    if (
        cover.value != '' &&
        book.value != '' &&
        bookName.value != '' &&
        author.value != '' &&
        language.value != '' &&
        yearOfEdition.value != '' &&
        publishingCompany.value != '' &&
        genreSelect.value != ''
    ) {
        document.getElementById('save').style.display = 'inline';
        document.getElementById('message').style.display = 'none';
    } else {
        document.getElementById('save').style.display = 'none';
        document.getElementById('message').style.display = 'inline';
    }
}

cover.addEventListener('change', function(e) {
    var file = e.target.files[0];
    var storageRef = firebase.storage().ref('covers/' + file.name);
    var task = storageRef.put(file);

    task.on('state_changed',
        function progress(snapshot) {
            var percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            coverProgress.value = percentage;
        },
        function error(err) {
            alert(error)
        },
        function complete() {
            task.snapshot.ref.getDownloadURL().then(function(coverDownloadURL) {
                document.getElementById('bookCover').value = coverDownloadURL;
                document.getElementById('coverPreviewBookImg').src = coverDownloadURL;
                console.log(coverPreviewBookImg);
            });
        }
    );
});

book.addEventListener('change', function(e) {
    var file = e.target.files[0];
    var storageRef = firebase.storage().ref('books/' + file.name);
    var task = storageRef.put(file);

    task.on('state_changed',
        function progress(snapshot) {
            var percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            bookProgress.value = percentage;
        },
        function error(err) {
            alert(error)
        },
        function complete() {
            task.snapshot.ref.getDownloadURL().then(function(bookDownloadURL) {
                document.getElementById('bookPdf').value = bookDownloadURL;
            });
        });
});

save.addEventListener("click", function(e) {
    create(
        bookName.value,
        serie.value,
        bookNumber.value,
        author.value,
        pages.value,
        language.value,
        yearOfEdition.value,
        publishingCompany.value,
        synopsis.value,
        bookCover.value,
        bookPdf.value,
        genreSelect.value,
    );
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function() { x.className = x.className.replace("show", ""); }, 2000);
    setTimeout(function() { document.location.reload(true); }, 1000);
})

function create(bookName, serie, bookNumber, author, pages, language, yearOfEdition,
    publishingCompany, synopsis, bookCover, bookPdf, genreSelect) {
    let data = {
        bookName: bookName,
        serie: serie,
        bookNumber: bookNumber,
        author: author,
        pages: pages,
        language: language,
        yearOfEdition: yearOfEdition,
        publishingCompany: publishingCompany,
        synopsis: synopsis,
        bookCover: bookCover,
        bookPdf: bookPdf,
        genreSelect: genreSelect,
    }
    return firebase.database().ref().child("books").push(data);
}
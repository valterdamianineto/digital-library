var btnLogin = document.getElementById('btnLogin');
var inputEmail = document.getElementById('inputEmail');
var inputPassword = document.getElementById('inputPassword');


// ****Função acionada ao carregar a pagina****
function hidden(inputPassword) {
    if (inputPassword == null) {
        document.getElementById('btnLogin').style.display = 'none';
        document.getElementById('canBtn').style.display = 'none';
    } else {
        document.getElementById('btnLogin').style.display = 'inline';
        document.getElementById('canBtn').style.display = 'inline';
    }
}
//****Função acionada ao inserir conteudo no campo de senha****
function reveal() {
    var revealPass = document.getElementById("inputPassword").value;
    var revealLog = document.getElementById("inputEmail").value;
    if (revealPass != '' && revealLog != '') {
        document.getElementById('btnLogin').style.display = 'inline';
        document.getElementById('canBtn').style.display = 'inline';
    } else {
        document.getElementById('btnLogin').style.display = 'none';
        document.getElementById('canBtn').style.display = 'none';
    }

}
//****Função acionada ao clicar no batão cancelar****
function refresh(canBtn) {
    document.location.reload(true);
}

btnLogin.addEventListener('click', function() {

    firebase.auth().signInWithEmailAndPassword(inputEmail.value, inputPassword.value).then(function(result) {
        console.log("Success!");
        window.location.replace('../home/home.html');
    }).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        document.location.reload(true);
        alert("O email ou a senha estão incorretos")
        console.log("Failure!")
    });

});

var input = document.getElementById("inputPassword");
input.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("btnLogin").click();
    }
});